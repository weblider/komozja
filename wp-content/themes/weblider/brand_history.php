<?php

/* Template name: Historia marki */

?>
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">

    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
    <style>
        .flickity-viewport{
            height: 100% !important;
        }
       .flickity-slider{
           display: flex;
           text-align: center;
           justify-content: center;
           align-items: center;
           align-content: center;
       }
        .single_repeater{
            height:100%;
        }
    </style>
<?php get_header(); ?>

<?php

$thumb__post = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');

?>

    <div class="thumb_fixed" style="background-image: url(<?php echo $thumb__post[0] ?>)">

        <section class="archive_header history_header">

            <div class="header_desc">

                <?php

                if (have_posts()) : ?>

                    <?php while (have_posts()) : the_post(); ?>

                        <h1 class="wow fadeInUp"><?php echo the_title(); ?></h1>

                        <div class="wow fadeInUp">
                            <?php echo the_content(); ?>
                        </div>

                    <?php endwhile; ?>

                <?php endif; ?>

            </div>

            <img src="<?php echo $thumb_cat['url'] ?>" alt="<?php echo single_cat_title() ?>">

        </section>

        <section class="history_body">

            <?php if (have_rows('page_content')): ?>

                <?php while (have_rows('page_content')): the_row();

                    $image_page = get_sub_field('image_page');

                    $year_page = get_sub_field('year_page');

                    $desc_page = get_sub_field('desc_page');

                    $full_section = get_sub_field('full_section');

                    $year_array = str_split($year_page, 1);

                    if ($full_section) { ?>

                        <div class="single_repeater full" id="<?php echo $year_page ?>">

                            <img class="img_full" src="<?php echo $image_page['url'] ?>"
                                 alt="<?php echo $image_page['alt'] ?>">

                            <div class="line">

                                <p class="year">

                                    <?php foreach ($year_array as $number) { ?>

                                        <span><?php echo $number ?></span>

                                    <?php } ?>

                                </p>

                            </div>

                            <div class="desc wow fadeInUp">

                                <?php echo $desc_page ?>

                            </div>

                        </div>

                    <?php } else {

                        if (get_row_index() % 2 == 0) { ?>

                            <div class="single_repeater odd" id="<?php echo $year_page ?>">

                                <div class="desc wow fadeInLeft">

                                    <?php echo $desc_page ?>

                                </div>

                                <div class="line">

                                    <p class="year">

                                        <?php foreach ($year_array as $number) { ?>

                                            <span><?php echo $number ?></span>

                                        <?php } ?>

                                    </p>

                                </div>
                                <?php if(get_sub_field('gallery_history_slider') == "true") { ?>
                                <div class="image">
                                    <div class="main-carousel " style="width:100%;" data-flickity='{ "cellAlign": "left", "contain": true, "pageDots":false}'>
                                        <?php foreach(get_sub_field('gallery') as $gallery_image) { ?>
                                            <div class="col">
                                                <img style="width:100%;"src="<?php echo $gallery_image['url'] ?>" alt="<?php echo $gallery_image['alt'] ?>">
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php } if(get_sub_field('gallery_history_slider') == "false") { ?>
                                    <div class="image">

                                        <img src="<?php echo $image_page['url'] ?>" alt="<?php echo $image_page['alt'] ?>">

                                    </div>
                                <?php } ?>


                            </div>

                        <?php } else { ?>

                            <div class="single_repeater even" id="<?php echo $year_page ?>">

                                <?php if(get_sub_field('gallery_history_slider') == "true") { ?>
                                    <div class="image">
                                        <div class="main-carousel " style="width:100%;" data-flickity='{ "cellAlign": "left", "contain": true, "pageDots":false}'>
                                            <?php foreach(get_sub_field('gallery') as $gallery_image) { ?>
                                                <div class="col">
                                                    <img style="width:100%;"src="<?php echo $gallery_image['url'] ?>" alt="<?php echo $gallery_image['alt'] ?>">
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } if(get_sub_field('gallery_history_slider') == "false") { ?>
                                    <div class="image">

                                        <img src="<?php echo $image_page['url'] ?>" alt="<?php echo $image_page['alt'] ?>">

                                    </div>
                                <?php } ?>

                                <div class="line">

                                    <p class="year">

                                        <?php foreach ($year_array as $number) { ?>

                                            <span><?php echo $number ?></span>

                                        <?php } ?>

                                    </p>

                                </div>
                                <div class="desc wow fadeInLeft">

                                    <?php echo $desc_page ?>

                                </div>


                            </div>

                        <?php }

                    }

                    ?>

                <?php endwhile; ?>

            <?php endif; ?>

        </section>

    </div>


<?php get_footer();

