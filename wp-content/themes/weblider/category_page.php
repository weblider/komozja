<?php

/* Template name: Kolekcje */

?>

<?php get_header(); ?>

 <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

<main>
    <section class="archive_header">
        <?php
            if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <div class="content wow fadeInUp">
                        <?php echo get_the_content(); ?>
                    </div>
                    <?php
                    if( !empty( $thumb ) ) { ?>
                    <img src="<?php echo $thumb['0']?>" alt="<?php echo get_the_title();?>">
                    <?php }
                    ?>
                <?php endwhile; ?>
            <?php endif; ?>
    </section>

    <div class="popular_post">
        <div class="grid">
        <?php
        $args = array(
            'taxonomy'   => "product_cat",
            'hide_empty' => true,
        );
        $product_categories = get_terms($args);
        $count = 0;
        foreach( $product_categories as $cat ) {
            $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
            $image = wp_get_attachment_url( $thumbnail_id );
            $count++;
            if( $cat->slug == 'bez-kategorii' ) {

            } else {
                if( $count == 1  || $count == 3) {
                    $class = 'down';

                } else {
                    $class = 'up';

                }
                ?>
                <div class="single_grid <?php echo $class ?> single_product_card">
                    <a href="<?php echo get_category_link($cat->term_id) ?>">
                        <div class="desc desc_up">

                        </div>
                        <img src="<?php echo $image ?>" alt="<?php echo $cat->name; ?>">
                        <div class="desc desc_down">
                            <h3><?php echo $cat->name; ?></h3>
                            <p><?php echo $cat->description; ?></p>
                        </div>
                    </a>
                </div>
            <?php }

            if( $count == 3 ) {
                $count = 0;
            }
            ?>
        <?php } ?>
        </div>
    </div>
</main>

<?php get_footer();

