(function ($) {
  $(document).ready(function () {
    // hard translate string
    var htmlLang = $("html").attr("lang");
    if (htmlLang == "en-GB") {
      var homeURL = $(".homePageBreadcrumb").find("a").attr("href");
      $(".homePageBreadcrumb")
        .find("a")
        .attr("href", homeURL + "/en");
    }
    if (htmlLang == "pl-PL") {
      setTimeout(function () {
        $(".table_Payment th").text("Metoda płatności");
        $("#billing_company_field label").text("Nazwa firmy (opcjonalnie)");
        $("#billing_address_1_field label").text("Adres ulicy");
        $("#shipping_address_1_field label").text("Adres ulicy");
        $("#billing_city_field label").text("Miasto");
        $("#shipping_city_field label").text("Miasto");
        $("#shipping_company_field label").text("Nazwa firmy (opcjonalnie)");
        $("#order_comments_field label").text(
          "Uwagi do zamówienia (opcjonalnie)"
        );
        $("#billing_address_2").attr(
          "placeholder",
          "Apartament, jednostka itp. (Opcjonalnie)"
        );
        $("#billing_address_1").attr("placeholder", "Numer domu i nazwa ulicy");
        $("#shipping_address_1").attr(
          "placeholder",
          "Apartament,jednostka itp. (Opcjonalnie)"
        );
        $("#shipping_address_2").attr(
          "placeholder",
          "Numer domu i nazwa ulicy"
        );
        $("#order_comments").attr(
          "placeholder",
          "Uwagi na temat zamówienia, np. specjalne uwagi dotyczące dostawy."
        );
      }, 500);
    }

    if (matchMedia("(max-width: 767px)").matches) {
      $(".all_cat .single_cat .desc_up").matchHeight();
    }
    if (matchMedia("(min-width: 769px)").matches) {
      var tl = gsap.timeline({
        scrollTrigger: {
          trigger: ".produkt_front_fixed",
          start: "center center",
          end: "150%",
          scrub: true,
          pin: true,
        },
      });
      tl.to(".single-product .scroll", {
        duration: 5,
        opacity: 0,
      });
      tl.to(".produkt_front_fixed .bg", {
        duration: 5,
        backgroundColor: "#FFF",
      });
      tl.to(".scroll_container .image", {
        duration: 5,
        "-webkit-filter": "grayscale(100%)",
        /* needed to work in webkit based browsers that don't support standards */
        filter: "grayscale(100%)",
      });
      tl.to(".single-product .desc_left", {
        duration: 5,
        opacity: 1,
        x: 0,
      });

      tl.to(".single-product .title_page", {
        duration: 5,
        opacity: 1,
        y: 0,
      });
      tl.to(".single-product .thumb_detail .height", {
        duration: 5,
        opacity: 1,
        y: 0,
      });
      tl.to(".single-product .thumb_detail .width", {
        duration: 5,
        opacity: 1,
        y: 0,
      });
      tl.to(".single-product .thumb_detail .length", {
        duration: 5,
        opacity: 1,
        y: 0,
      });
      tl.to(".single-product .code_product", {
        duration: 15,
        opacity: 1,
        x: 0,
      });

      // var tl2 = gsap.timeline({
      //     scrollTrigger: {
      //         trigger: ".single_box.content_left",
      //         start: "center 70%",
      //         end: "center bottom",
      //         scrub: true,
      //         pin: true,
      //         markser: true
      //     }
      // });
      // tl2.to('.single_box.content_left .bg', {
      //     backgroundPosition: '129% 100%',
      // });
      // var tl3 = gsap.timeline({
      //     scrollTrigger: {
      //         trigger: ".single_box.content_right",
      //         start: "center 70%",
      //         end: "center bottom",
      //         scrub: true,
      //         pin: true,
      //         markser: true
      //     }
      // });
      // tl3.to('.single_box.content_right .bg', {
      //     backgroundPosition: '-29% 100%',
      // });
    }

    $(".single-product .scroll").on("click", function (e) {
      $("html, body").scrollTop(0); //Czas po ilu sekundach po wejściu na stronę na scrollować w dół
      $("html, body").animate(
        {
          scrollTop:
            $(".pin-spacer")[0].scrollHeight -
            $(".pin-spacer")[0].scrollHeight / 2,
        },
        1000
      ); // Szybkośc animacji
    });

    $(window).scroll(function () {
      var scroll = $(window).scrollTop();
      if (scroll >= 150) {
        $("header").addClass("header-fixed");
      } else {
        $("header").removeClass("header-fixed");
      }
    });

    if (window.location.hash) {
      setTimeout(function () {
        $("html, body").scrollTop(0).delay(500); //Czas po ilu sekundach po wejściu na stronę na scrollować w dół
        $("html, body").animate(
          {
            scrollTop: $(window.location.hash).offset().top - 100, // Offset od elementu, bez offsetu zostawić ".top"
          },
          1000
        ); // Szybkośc animacji
      }, 0);
    }

    $(function () {
      function rescaleCaptcha() {
        var width = $(".g-recaptcha").parent().width();
        var scale;
        if (width < 302) {
          scale = width / 302;
        } else {
          scale = 1.0;
        }

        $(".g-recaptcha").css("transform", "scale(" + scale + ")");
        $(".g-recaptcha").css("-webkit-transform", "scale(" + scale + ")");
        $(".g-recaptcha").css("transform-origin", "0 0");
        $(".g-recaptcha").css("-webkit-transform-origin", "0 0");
      }

      rescaleCaptcha();
      $(window).resize(function () {
        rescaleCaptcha();
      });
    });

    $(".scrollHref a").on("click", function (e) {
      e.preventDefault();
      var target = $(this).attr("href");
      $("html, body").animate(
        {
          scrollTop: $(target).offset().top,
        },
        1000
      );
    });

    $(".header__content .search img").click(function () {
      $(".header__content .search form").animate({
        width: "toggle",
      });
    });
    $(".header__content .down_content .cart .cart_btn").click(function () {
      $(".header__content .down_content .cart .basket_list").slideToggle();
    });

    // Hamburger mobile menu
    $(".js-hamburger").click(function (event) {
      event.preventDefault();
      $("body").toggleClass("mobile__menu--open");
      $(this).toggleClass("is-active");
    });

    $(".page_item_has_children a").click(function (e) {
      e.preventDefault();
      const child = $(this).parent().find(".children").first();
      child.slideToggle();
      $(this).parent().toggleClass("open");
    });
    // lightcase
    $("a[data-rel^=lightcase]").lightcase({
      swipe: true,
      maxWidth: 1400,
      maxHeight: 900,
      shrinkFactor: 0.9,
    });
    // MatchHeight
    $(".single_tag_img").matchHeight();

    // Animate wow + animate.css
    var wow = new WOW({
      boxClass: "wow", // animated element css class (default is wow)
      animateClass: "animated", // animation css class (default is animated)
      offset: 300, // distance to the element when triggering the animation (default is 0)
      mobile: false, // trigger animations on mobile devices (default is true)
      live: true, // act on asynchronously loaded content (default is true)
      scrollContainer: null, // optional scroll container selector, otherwise use window
    });
    wow.init();

    // Swiper
    var swiper = new Swiper(".swiper_collections", {
      slidesPerView: 3,
      spaceBetween: 30,
      centeredSlides: true,
      loop: true,
      speed: 1000,
      autoplay: {
        delay: 4500,
        disableOnInteraction: false,
      },
      navigation: {
        nextEl: ".collections_container .swiper-button-next",
        prevEl: ".collections_container .swiper-button-prev",
      },
      breakpoints: {
        768: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        450: {
          slidesPerView: 1,
          spaceBetween: 30,
        },
      },
    });
    var swiper2 = new Swiper(".swiper_collection", {
      slidesPerView: 2,
      spaceBetween: 5,
      centeredSlides: true,
      loop: true,
      autoplay: {
        delay: 9500,
        disableOnInteraction: false,
      },
      navigation: {
        nextEl: ".collection_container .swiper-button-next",
        prevEl: ".collection_container .swiper-button-prev",
      },
      breakpoints: {
        768: {
          slidesPerView: 1.7,
          spaceBetween: 5,
        },
        640: {
          slidesPerView: 1.5,
          spaceBetween: 5,
        },
        450: {
          slidesPerView: 1.2,
          spaceBetween: 5,
        },
      },
    });
    var swiper3 = new Swiper(".swiper_stages", {
      slidesPerView: 1,
      spaceBetween: 5,
      loop: true,
      autoplay: {
        delay: 9500,
        disableOnInteraction: false,
      },
      navigation: {
        nextEl: ".stages .swiper-button-next",
        prevEl: ".stages .swiper-button-prev",
      },
    });

    var swiper4 = new Swiper(".swiper_stages2", {
      slidesPerView: 1,
      spaceBetween: 5,
      loop: true,
      autoplay: {
        delay: 9500,
        disableOnInteraction: false,
      },
      navigation: {
        nextEl: ".stages2 .swiper-button-next",
        prevEl: ".stages2 .swiper-button-prev",
      },
    });

    var nextSlide = $(".stages2 .swiper-slide-next .image")
      .find(".current_image")
      .attr("src");
    var prevSlide = $(".stages2 .swiper-slide-prev .image")
      .find(".current_image")
      .attr("src");
    $(".stages2 .img_left").attr("src", prevSlide);
    $(".stages2 .img_right").attr("src", nextSlide);
    if ($(".swiper_stages2").length) {
      var slideLength = swiper4.slides.length;
    }

    swiper4.on("transitionEnd ", function () {
      $(".stages .bg .img_left").css("left", "0px");
      $(".stages .bg .img_right").css("right", "0px");
      setTimeout(function () {
        var currentSlideIndex = swiper4.realIndex;
        var prevID = currentSlideIndex - 1;
        var nextID = currentSlideIndex + 1;
        if (prevID == -1) {
          prevID = 2;
        }
        if (nextID == 3) {
          nextID = 0;
        }
        var nextSlide = $(
          ".stages2 .id_" +
            nextID +
            ':not(div[class^="swiper-slide-duplicate"])'
        )
          .find(".current_image")
          .attr("src");
        var prevSlide = $(
          ".stages2 .id_" +
            prevID +
            ':not(div[class^="swiper-slide-duplicate"])'
        )
          .find(".current_image")
          .attr("src");
        $(".stages2 .img_right").attr("src", nextSlide);
        $(".stages2 .img_left").attr("src", prevSlide);

        $(".stages2 .bg .img_left").css("left", "-100px");
        $(".stages2 .bg .img_right").css("right", "-100px");
      }, 1000);
    });

    var nextSlide = $(".stages .swiper-slide-next .image")
      .find(".current_image")
      .attr("src");
    var prevSlide = $(".stages .swiper-slide-prev .image")
      .find(".current_image")
      .attr("src");
    $(".stages .img_left").attr("src", prevSlide);
    $(".stages .img_right").attr("src", nextSlide);
    if ($(".swiper_stages").length) {
      var slideLength = swiper3.slides.length;
    }

    swiper3.on("transitionEnd ", function () {
      $(".stages .bg .img_left").css("left", "0px");
      $(".stages .bg .img_right").css("right", "0px");
      setTimeout(function () {
        var currentSlideIndex = swiper3.realIndex;
        var prevID = currentSlideIndex - 1;
        var nextID = currentSlideIndex + 1;
        if (prevID == -1) {
          prevID = 2;
        }
        if (nextID == 3) {
          nextID = 0;
        }
        var nextSlide = $(
          ".stages .id_" + nextID + ':not(div[class^="swiper-slide-duplicate"])'
        )
          .find(".current_image")
          .attr("src");
        var prevSlide = $(
          ".stages .id_" + prevID + ':not(div[class^="swiper-slide-duplicate"])'
        )
          .find(".current_image")
          .attr("src");
        $(".stages .img_right").attr("src", nextSlide);
        $(".stages .img_left").attr("src", prevSlide);

        $(".stages .bg .img_left").css("left", "-250px");
        $(".stages .bg .img_right").css("right", "-250px");
      }, 1000);
    });

    // End document.ready
  });
})(jQuery);
