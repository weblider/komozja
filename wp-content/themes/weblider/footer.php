<?php
    $logo_footer = get_field( 'logo_footer',  pll_current_language('slug') );
    $title_1 = get_field( 'title_1',  pll_current_language('slug') );
    $title_2 = get_field( 'title_2',  pll_current_language('slug') );
    $title_3 = get_field( 'title_3',  pll_current_language('slug') );
    $desc_3 = get_field( 'desc_3',  pll_current_language('slug') );
    $title_4 = get_field( 'title_4',  pll_current_language('slug') );
    $icon_payu = get_field( 'icon_payu',  pll_current_language('slug') );
    $desc_payu = get_field( 'desc_payu',  pll_current_language('slug') );
?>
<footer>
    <div class="single col_1">
        <div class="logo_footer">
            <img  src="<?php echo $logo_footer['url'] ?>" alt="<?php echo $logo_footer['alt'] ?>">
        </div>

        <div class="copyright">
          <p><?php echo pll_e( 'Projekt strony:' );?></p>
          <a class="weblider" target="_blank" href="http://www.weblider.eu">  <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/weblider.png" alt="weblider"></a>
        </div>
    </div>
    <div class="single col_2">
        <h3><?php echo $title_1 ?></h3>
        <?php
        $posts = get_field('page_1', pll_current_language('slug'));
        if( $posts ): ?>
        	<ul>
        	<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
        	    <li>
        	    	<a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a>
        	    </li>
        	<?php endforeach; ?>
        	</ul>
        <?php endif; ?>
    </div>
    <div class="single col_3">
        <h3><?php echo $title_2 ?></h3>
        <?php
        $posts2 = get_field('page_2', pll_current_language('slug'));
        if( $posts2 ): ?>
            <ul>
            <?php foreach( $posts2 as $p2 ): // variable must NOT be called $post (IMPORTANT) ?>
                <li>
                    <a href="<?php echo get_permalink( $p2->ID ); ?>"><?php echo get_the_title( $p2->ID ); ?></a>
                </li>
            <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
    <div class="single col_4">
        <h3><?php echo $title_3 ?></h3>
        <?php echo $desc_3 ?>
    </div>
    <div class="single col_5">
        <h3><?php echo $title_4 ?></h3>
        <?php
        if( is_user_logged_in()  ) { ?>
            <div class="btn">
                <a class="logout" href="<?php echo get_permalink( pll_get_post(get_option('woocommerce_myaccount_page_id')) ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php echo pll_e('Moje konto'); ?></a>
            </div>
            <div class="btn">
                <a class="logout" role="button" href="<?php echo wp_logout_url( home_url() ); ?>"><?php echo pll_e( 'Wyloguj się' );?></a>
            </div>
        <?php } else { ?>
            <form class="woocommerce-form woocommerce-form-login login" method="post">

                <?php do_action( 'woocommerce_login_form_start' ); ?>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" placeholder="<?php echo pll_e('Login')?>"  value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                </p>
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" placeholder="<?php echo pll_e('Hasło')?>" id="password" autocomplete="current-password" />
                </p>

                <?php do_action( 'woocommerce_login_form' ); ?>

                <p class="form-row">
                    <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
                    <button type="submit" class="woocommerce-button button woocommerce-form-login__submit" name="login" value="<?php echo pll_e('Zaloguj się' ); ?>"><?php echo pll_e('Zaloguj się' ); ?></button>
                </p>
                <?php do_action( 'woocommerce_login_form_end' ); ?>

            </form>
        <?php }
        ?>
    </div>
    <div class="single col_6">
        <?php
        if( !empty( $icon_payu ) ) { ?>
            <img src="<?php echo $icon_payu['url'] ?>" alt="<?php echo $icon_payu['alt'] ?>">
        <?php }
        ?>
        <?php echo $desc_payu ?>
    </div>

</footer>
<div class="drawer-overlay drawer-toggle">

</div>
<?php wp_footer(); ?>
</body>
</html>
