<?php

add_theme_support( 'post-thumbnails' );

add_post_type_support( 'page', 'excerpt' );

add_filter( 'wpcf7_autop_or_not', '__return_false' );





/* Add to the functions.php file of your theme */

add_filter( 'woocommerce_order_button_text', 'woo_custom_order_button_text' );



function woo_custom_order_button_text() {

    return __( 'Zamawiam', 'woocommerce' );

}









add_action('after_setup_theme', 'wpdocs_theme_setup');

/**

 * Load translations for wpdocs_theme

 */

function wpdocs_theme_setup(){

    load_theme_textdomain('wpdocs_theme', get_template_directory() . '/languages');

}





function my_woocommerce_make_tags_hierarchical( $args ) {

    $args['hierarchical'] = true;

    return $args;

};

add_filter( 'woocommerce_taxonomy_args_product_tag', 'my_woocommerce_make_tags_hierarchical' );



add_filter( 'woocommerce_get_order_item_totals', 'adjust_woocommerce_get_order_item_totals' );



function adjust_woocommerce_get_order_item_totals( $totals ) {

  unset($totals['cart_subtotal']  );

  return $totals;

}

add_filter( 'woocommerce_account_menu_items', 'custom_remove_downloads_my_account', 999 );



function custom_remove_downloads_my_account( $items ) {

unset($items['downloads']);

unset($items['dashboard']);

return $items;

}

// function wpcustom_deregister_scripts_and_styles(){

//     wp_deregister_style('storefront-woocommerce-style');

//     wp_deregister_style('storefront-style');

// }

// add_action( 'wp_print_styles', 'wpcustom_deregister_scripts_and_styles', 100 );







add_action( 'after_setup_theme', 'woocommerce_support' );

function woocommerce_support() {

    add_theme_support( 'woocommerce' );

}





/**

 * Show cart contents / total Ajax

 */

add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );



function woocommerce_header_add_to_cart_fragment( $fragments ) {

	global $woocommerce;



	ob_start();



	?>

	<a class="cart-customlocation" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>

	<?php

	$fragments['a.cart-customlocation'] = ob_get_clean();

	return $fragments;

}





// category id in body and post class

function category_id_class($classes) {

	global $post;

	foreach((get_the_category($post->ID)) as $category)

		$classes [] = 'cat-' . $category->cat_ID . '-id';

		return $classes;

}

add_filter('post_class', 'category_id_class');

add_filter('body_class', 'category_id_class');





@ini_set( 'upload_max_size' , '64M' );

@ini_set( 'post_max_size', '64M');

@ini_set( 'max_execution_time', '300' );

//Register Navigations

add_action( 'init', 'my_custom_menus' );

function my_custom_menus() {

    register_nav_menus(

        array(

            'primary-menu' => __( 'Primary Menu' )

        )

    );

}

// Stylesheet and Script



function style_script() {

  wp_enqueue_style( 'plugin-css', get_stylesheet_directory_uri() . '/dist/css/plugin.min.css', '', filemtime(get_stylesheet_directory() . '/dist/css/plugin.min.css' ) );

  wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/dist/css/main.css', '', filemtime(get_stylesheet_directory() . '/dist/css/main.css' ));

  wp_enqueue_style( 'styleWordpress', get_stylesheet_directory_uri() . '/style.css', '', filemtime(get_stylesheet_directory() . '/style.css' ));

  wp_enqueue_style( 'googlefonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,700,800', '', '', true  );

    // Scripts

  wp_deregister_script('jquery-script');

    wp_deregister_script('jquery');

    wp_register_script('jquery', '//code.jquery.com/jquery-3.2.1.min.js', array(), null);

    wp_enqueue_script('jquery');



  wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/dist/js/scripts.js', array( 'jquery' ), filemtime(get_stylesheet_directory() . '/dist/js/scripts.js' ), false );



    wp_localize_script( 'script', 'rest_url', array(

      'root' => esc_url_raw( rest_url() ),

      'theme' => get_stylesheet_directory_uri(),

      'ajax' => admin_url( 'admin-ajax.php' ),

  ) );



}

add_action( 'wp_enqueue_scripts', 'style_script' );



add_action( 'after_setup_theme', 'fh_editor_style' );

function fh_editor_style(){

  add_theme_support( 'editor-styles' );

  add_editor_style( 'dist/css/editor.css' );

}



if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(

		'page_title' 	=> 'Header',

		'menu_title'	=> 'Nagłówek',

		'menu_slug' 	=> 'header',

		'capability'	=> 'edit_posts',

		'redirect'		=> false

		));



		$parent = acf_add_options_page( array(

		  'page_title' => 'Stopka',

		  'menu_title' => 'Stopka',

		  'redirect'   => 'Stopka',

		   ) );



		   $languages = array( 'pl', 'en' );

   foreach ( $languages as $lang ) {

	 acf_add_options_sub_page( array(

	 'page_title' => 'Footer (' . strtoupper( $lang ) . ')',

	 'menu_title' => __('Stopka (' . strtoupper( $lang ) . ')', 'text-domain'),

	 'menu_slug'  => "footer-${lang}",

	 'post_id'    => $lang,

	  'parent'     => $parent['menu_slug']

	 ) );

   }

		$parent = acf_add_options_page( array(

		  'page_title' => 'Ustawienia globalne',

		  'menu_title' => 'Ustawienia globalne',

		  'redirect'   => 'Ustawienia globalne',

		   ) );



		   $languages = array( 'pl', 'en' );

   foreach ( $languages as $lang ) {

	 acf_add_options_sub_page( array(

	 'page_title' => 'global (' . strtoupper( $lang ) . ')',

	 'menu_title' => __('Ustawienia globalne (' . strtoupper( $lang ) . ')', 'text-domain'),

	 'menu_slug'  => "global-${lang}",

	 'post_id'    => $lang,

	  'parent'     => $parent['menu_slug']

	 ) );

   }



}

// require get_template_directory().'/inc/jsdefer.php';

// require get_template_directory().'/inc/wpHtmlCompression.php';

// require get_template_directory().'/inc/ajax.php';

// require get_template_directory().'/inc/breadcrumb.php';

// require get_template_directory().'/inc/pagination.php';

// require get_template_directory().'/inc/sitemap.php';



// Widget

register_sidebar( array (

'name' => __( 'Widget'),

'id' => 'lang-widget',

'before_widget' => '',

'after_widget' => '',

'before_title' => '',

'after_title' => '',

) );

register_sidebar( array (

'name' => __( 'Polylang Mobile'),

'id' => 'polylang-mobile',

'before_widget' => '',

'after_widget' => '',

'before_title' => '',

'after_title' => '',

) );





// Skróty

function tempDir() {

    return get_template_directory_uri();

}



function imageDir( $string ) {

    return tempDir() . '/img/' . $string;

}





function fileDir() {

    return get_template_directory();

}

/**

 *  Podpinanie lightcase pod galerie

 */



    function get_id($inc = false) {

      static $id;

        if ($inc) {

          $id++;

        }

      return $id;

    }



    function replace($link) {

      $id = get_id();

      return str_replace('<a href=', '<a data-rel="lightcase:Gallery-'.$id.'" href=', $link);

    }



    add_filter(

      'post_gallery',

      function() {

        get_id(true);

        add_filter('wp_get_attachment_link','replace');

      }

    );



    function taco_gallery_default_settings( $settings ) {

    $settings['galleryDefaults']['link'] = 'file';

    return $settings;

}

add_filter( 'media_view_settings', 'taco_gallery_default_settings');



function phone_to_link( $string ) {

    $string = str_replace( '+', '00', $string );

    $string = str_replace( '-', '', $string );

    $string = str_replace( ' ', '', $string );

    $string = str_replace( '.', '', $string );

    return $string;

}

//<a href="tel:php echo esc_html(phone_to_link( $number_tel ) ); "> </a>





// LOGIN PAGE

function my_custom_login_logo() {

     echo '<style type="text/css">

   body {

    background-image: linear-gradient(to top, #537895 0%, #09203f 100%)

   }

   .login h1 a {

    height: 57px!important;

    width: 213px!important;

    background-position: center center;

    background-repeat: no-repeat;

    background-size: 100% auto;

     background-image: url( https://weblider.eu/wp-content/themes/taco/img/logo_biale@2.png )!important;

     margin-bottom: 0px;

  }

  .login h1:after {

    content: "Strony internetowe, identyfikacja wizualna firmy, kampanie marketingowe Google AdWords.";

    display:block;

    color: #FFF;

    font-size: 13px;

  }

  .login h1 {

    border-bottom: 1px solid #bdbdbd;

    padding-bottom: 20px;

  }

  .login form {

    background-color: rgba( 255,255,255,0.10 );

  }

  .login form .input, .login form input[type=checkbox], .login input[type=text] {

    background-color: rgba( 0,0,0,0 );

    color: #FFF;

    border: 1px solid ( 255,255,255,0.4 );

  }

  .login label{

    color: #FFF;

  }

  .login #backtoblog a, .login #nav a  {

    color: #FFF;

  }

  #login form p {

    text-align: center;

  }

  .login #login_error, .login .message, .login .success {

    color: #FFF;

    background-color: rgba( 255,255,255,0.14 );

  }

  a {

    color: #FFF;

    text-decoration: underline!important;

    }



     </style>';

  }



add_action('login_head', 'my_custom_login_logo');



add_filter( 'login_headerurl', 'custom_loginlogo_url' );



function custom_loginlogo_url($url) {



     return 'http://www.weblider.eu';



}
