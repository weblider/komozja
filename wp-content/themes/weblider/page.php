<?php get_header(); ?>
<section class="archive_header cart_header">
    <div class="header_desc">
        <h1><?php echo get_the_title() ?></h1>
    </div>
</section>
<div class="content-wrap">
    <div class="content container">
        <div class="row">
            <div class="col-12 breadcrumbs_container">
            <?php bcn_display($return = false, $linked = true, $reverse = false, $force = false) ?>
            </div>
            <?php
            if (is_cart() ) { ?>
             <div class="cart_template col-12">
                 <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                      if(has_post_format('link')):
                      else : ?>

                         <?php the_content(); ?>

                     <?php endif;
                     endwhile;
                     else: ?>
                     <p><?php echo esc_html('Brak wpisów.'); ?></p>
               <?php endif; ?>
             </div>
         <?php } else if( is_account_page() ) { ?>
             <div class="account_template col-12">
                 <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                      if(has_post_format('link')):
                      else : ?>

                         <?php the_content(); ?>

                     <?php endif;
                     endwhile;
                     else: ?>
                     <p><?php echo esc_html('Brak wpisów.'); ?></p>
               <?php endif; ?>
             </div>
         <?php } else if( is_checkout() ) { ?>
             <div class="checkout_template col-12">
                 <?php
                     if ( have_posts() ) : ?>
                         <?php while ( have_posts() ) : the_post(); ?>
                               <?php the_content(); ?>
                         <?php endwhile; ?>
                     <?php endif; ?>
             </div>
         <?php }  else { ?>
              <div class="page-content col-12">
                  <?php
                      if ( have_posts() ) : ?>
                          <?php while ( have_posts() ) : the_post(); ?>
                                <?php the_content(); ?>
                          <?php endwhile; ?>
                      <?php endif; ?>
             </div>
          <?php }
            ?>
        </div>
    </div>
</div>
<?php get_footer();
