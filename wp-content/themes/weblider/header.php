<!DOCTYPE HTML>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php bloginfo('name'); ?> <?php if ( is_single() || is_page() ) { ?> - <?php the_title(); } ?> </title>
    <meta name="Description" content="<?php bloginfo('description'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/favicon.ico" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
       <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <?php wp_head();?>
</head>
<body <?php body_class(); ?>>
	<?php
	$logo_header = get_field( 'logo_header', 'options' );
	?>
    <div class="hamburger js-hamburger">
      <div class="hamburger__bar"></div>
    </div>
    <div class="mobile__menu">
        <nav class="mobile__nav" >
            <div class="lang">
                <p><?php echo pll_e('Language:'); ?></p>
                  <ul><?php pll_the_languages();?></ul>
            </div>
            <?php $defaults = array(
                'theme_location'  => 'primary-menu',
                'container'       => false,
                'menu_class'      => 'nav-menus',
                'menu_id'         => '',
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                'depth'      => '1',
            );
            wp_nav_menu( $defaults ); ?>
            <div class="search">
                 <?php get_search_form(); ?>
            </div>
        </nav>
    </div>
<header>
  <div class="container-fluid">
    <div class="row">
      <div class="col-5 col-md-9 header__navigation">
          <div class="header__logo">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                  <img src="<?php esc_html_e( $logo_header['url'] ); ?>" alt="logo">
              </a>
          </div>
          <?php
          $menu_name = 'primary-menu';
          $locations = get_nav_menu_locations();
          $menus = wp_get_nav_menu_object( $locations[ $menu_name ] );
          $array_menu = wp_get_nav_menu_items( $menus->term_id, array( 'order' => 'DESC' ) );
          $menu = array();
          foreach ($array_menu as $m) {
          if (empty($m->menu_item_parent)) {
              $menu[$m->ID] = array();
              $menu[$m->ID]['ID'] = $m->ID;
              $menu[$m->ID]['title'] = $m->title;
              $menu[$m->ID]['url'] = $m->url;
              $menu[$m->ID]['children'] = array();
          }
          }
          $submenu = array();
          foreach ($array_menu as $m) {
          if ($m->menu_item_parent) {
              if( $m->object == 'product_tag' ) {
                  $icon_tag = get_field( 'icon_tag', 'term_' . $m->object_id );
                  $image = $icon_tag['url'];
              } else  {
                  $thumbnail_id = get_woocommerce_term_meta( $m->object_id, 'thumbnail_id', true );
                  $image = wp_get_attachment_url( $thumbnail_id );
              }
              $submenu[$m->ID] = array();
              $submenu[$m->ID]['ID'] = $m->ID;
              $submenu[$m->ID]['title'] = $m->title;
              $submenu[$m->ID]['description'] = $m->description;
              $submenu[$m->ID]['image'] = $image;
              $submenu[$m->ID]['url'] = $m->url;
              $menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
          }
          }
          ?>
          <div class="navigation">
              <ul class="main-nav">
                  <?php foreach( $menu as $parent ) {
                      $active = '';
                      $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                          if($parent['url'] == $actual_link)
                          {
                               $active = 'active';
                          }
                          else
                          {
                              $active = '';
                          }
                          if( count($parent['children']) > 0 ) {
                              $haveChildren = 'haveChildren';
                          } else {
                              $haveChildren = '';
                          }
                      ?>
                      <li class="<?php echo $active ?> <?php echo $haveChildren ?>">
                              <a href="<?php echo $parent['url']?>"><?php echo $parent['title']?></a>
                              <?php if( count($parent['children']) > 0 ) { ?>
                                  <ul class="sub-menu ">
                                      <?php
                                      foreach( $parent['children'] as $children ) { ?>
                                          <li class="item  sub-menu--item">
                                              <a href="<?php echo $children['url']; ?>" class="title">
                                                  <div class="thumb">
                                                      <?php if( !empty( $children['image'] ) ){ ?>
                                                          <img src="<?php echo $children['image'] ?>" alt="<?php echo $children['title'] ?>">
                                                      <?php }?>
                                                  </div>
                                                  <div class="desc">
                                                      <p class="title"><?php echo $children['title'] ?></p>
                                                      <?php
                                                      if( !empty( $children['description'] ) ) { ?>
<!--                                                          <p class="small_desc">--><?php //echo wp_trim_words( $children['description'], 15, '...' );  ?><!--</p>-->
                                                      <?php }
                                                      ?>
                                                  </div>
                                              </a>
                                          </li>
                                      <?php }
                                      ?>
                                  </ul>
                              <?php } else {

                              }?>
                      </li>
                  <?php } ?>
              </ul>
          </div>

      </div>
      <div class="col-7 col-md-3 header__content">
          <div class="lang">
                <ul><?php pll_the_languages();?></ul>
          </div>
          <div class="down_content">
              <div class="search">
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ikona-wyszukiwarka@2.png" />
                   <?php get_search_form(); ?>
              </div>
              <div class="cart">
                    <div class="cart_btn">
                        <img class="cart_img" src="<?php echo get_stylesheet_directory_uri(); ?>/img/ikona-koszyk@2.png" />
                        <img class="cart_arrow" src="<?php echo get_stylesheet_directory_uri(); ?>/img/ikona-koszyk-strzalka@2.png" />
                    </div>
                    <?php echo WC()->cart->get_cart_total(); ?>
                  <div class="basket_list">
                      <ul>
                          <?php
                              global $woocommerce;
                              $items = $woocommerce->cart->get_cart();
                                if( !empty( $items ) ) {
                                    foreach($items as $item => $values) {
                                        $_product =  wc_get_product( $values['data']->get_id() );
                                        $getProductDetail = wc_get_product( $values['product_id'] );
                                        $price = get_post_meta($values['product_id'] , '_price', true);
                                        ?>
                                        <li>
                                            <p class="title"><a href="<?php echo get_page_link( $values['product_id'] ); ?>"><?php echo $_product->get_title() ?></a></p>
                                            <p class="quantity"><?php echo pll_e( 'Ilość:' );?> <span><?php echo $values['quantity'] ?></span></p>
                                            <p class="price"><?php echo pll_e( 'Kwota:' );?> <span><?php echo $price ?> <?php echo get_woocommerce_currency_symbol(); ?></span></p>
                                        </li>
                                        <?php
                                    } ?>
                                    <div class="btn">
                                        <!-- <a href="<?php echo wc_get_cart_url(); ?>"><?php echo pll_e( 'Koszyk' ); ?></a> -->
                                        <a href="<?php echo get_page_link(pll_get_post(8)); ?>"><?php echo pll_e( 'Koszyk' ); ?></a>
                                    </div>
                                    <?php
                                    if( is_user_logged_in() ) { ?>
                                        <div class="btn">
                                            <a role="button" href="<?php echo wp_logout_url( pll_home_url() ); ?>"><?php echo pll_e( 'Wyloguj się' );?></a>
                                        </div>
                                    <?php } else { ?>
                                        <div class="btn">
                                            <a href="<?php echo get_permalink( pll_get_post(get_option('woocommerce_myaccount_page_id')) ); ?>"><?php echo pll_e( 'Zaloguj się' );?></a>
                                        </div>
                                    <?php }
                                    ?>
                                <?php } else {
                                    ?>
                                    <p><?php echo pll_e( 'Twój koszyk jest pusty' );?></p>
                                    <?php
                                }
                          ?>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</header>
