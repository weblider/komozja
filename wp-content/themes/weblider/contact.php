<?php
/* Template name: Kontakt */
?>
<?php
    $thumb__post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
    $left_column = get_field( 'left_column' );
    $left_column_icon = get_field( 'left_column_icon' );
    $right_column = get_field( 'right_column' );
    $right_column_icon = get_field( 'right_column_icon' );
    $form_text = get_field( 'form_text' );
    $title_page = get_field( 'title_page' );
?>
<?php get_header(); ?>
<div class="full_thumb" style="background-image: url(<?php echo $thumb__post[0]?>)">
    <h1><?php echo the_title(); ?></h1>
</div>
<div class="page_content page_contact">
    <div class="breadcrumbs_container">
    <?php bcn_display($return = false, $linked = true, $reverse = false, $force = false) ?>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="row full_height">
                    <div class="col-md-6 left_column">
                        <div class="bg" style="background-image: url(<?php echo $left_column_icon['url']?>)">
                            <?php echo $left_column ?>
                        </div>
                    </div>
                    <div class="col-md-6 right_column">
                        <div class="bg" style="background-image: url(<?php echo $right_column_icon['url']?>)">
                            <?php echo $right_column ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 contact_form">
                <h2><?php echo $form_text ?></h2>
                <?php
                $lang = get_locale();
                if( $lang == "en_GB" ) {
                    echo do_shortcode('[contact-form-7 id="87" title="Contact form 1_ENG"]');
                } else {
                    echo do_shortcode('[contact-form-7 id="86" title="Contact form 1"]');
                }
                ?>
            </div>
            <div class="col-12 section_title">
                <h2><?php echo $title_page ?></h2>
            </div>
            <?php if( have_rows('page_loop') ): ?>
               <?php while( have_rows('page_loop') ): the_row();
                   $title_box = get_sub_field('title_box');
                   $icon_box = get_sub_field('icon_box');
                   $link_page = get_sub_field('link_page');
                    ?>
                      <div class="col-md-3 page_loop--single">
                           <a href="<?php echo $link_page ?>">
                              <div class="border" style="background-image: url(<?php echo $icon_box['url']?>)">
                                    <?php echo $title_box ?>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ikona-wymiar-szerokosc2@2.png" alt="strzałka" />
                              </div>
                           </a>
                      </div>
              <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
