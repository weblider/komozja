<?php
/* Template name: Front-page */
?>
<?php get_header(); ?>
<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
<style>
    .btn-kolekcje {
        font-size: 0.8125rem;
        text-transform: uppercase;
        color: #FFF;
        padding: 10px 30px;
        background-color: #181818;
        display: block;
        max-width: 220px;
        text-align: center;
        margin: 0 auto;
        letter-spacing: 3px;
        position: relative;
        overflow: hidden;
    }
</style>
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
<?php
$bg_start = get_field('bg_start');
$image_start = get_field('image_start');
$desc_start = get_field('desc_start');
global $product;
?>
<main>
    <section class="front">
        <img class="front_bg" src="<?php echo $bg_start['url'] ?>" alt="front_bg">
        <?php
        if (!empty($image_start)) { ?>
            <img class="image_front" src="<?php echo $image_start['url'] ?>" alt="<?php echo $image_start['alt'] ?>">
        <?php }
        ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">

                </div>
                <div class="col-md-4 front_desc wow fadeInRight">
                    <?php echo $desc_start ?>
                </div>
            </div>
        </div>
        <div class="scroll scrollHref">
            <a href="#history_container">
                <?php echo pll_e('Poznaj nas'); ?>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/strzalka@2.png" />
            </a>
        </div>
    </section>
    <?php
    $desc_collections = get_field('desc_collections');
    ?>
    <?php
    $args = array(
        'taxonomy'   => "product_cat",
        'hide_empty' => true,
    );
    $product_categories = get_terms($args);
    ?>

    <?php
    // var_dump($product_categories);

    $foo = 1;
    foreach ($product_categories as &$p) {
        $foo2[$foo] = $p;
        $foo++;
    }

    ?>

    <section class="stages2">
        <div class="container-fluid">
            <div class="row">
                <!--                <div class="col-12 title_section wow fadeInUp">-->
                <!--                    <h2>Wyjątkowe kolekcje</h2>-->
                <!--                </div>-->
                <div class="col-12">
                    <div class="bg">
                        <img class="img_left" src="" alt="slider_prev">

                        <?php if (have_rows('box_stages')) : $count = -1; ?>
                            <div class="swiper-container swiper_stages2">
                                <div class="swiper-wrapper">
                                    <?php foreach ($foo2 as $key => $cat) {
                                        $thumbnail_id = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
                                        $term_link = get_term_link($cat->term_id);
                                        $image = wp_get_attachment_url($thumbnail_id);
                                        $count++;



                                    ?>
                                        <?php if ($image) { ?>

                                            <?php $previous = array_key_exists($key - 1, $foo2) ? $foo2[$key - 1] : array_values($foo2)[0];
                                            $next = array_key_exists($key + 1, $foo2) ? $foo2[$key + 1] : array_values($foo2)[0]; ?>

                                            <div class="swiper-slide id_<?php echo $count ?>">
                                                <div class="title_section">
                                                    <h4 style="text-align: center;font-family: 'IM Fell English', serif;
                                                        font-style: italic;">Wyjątkowe kolekcje</h4>
                                                    <h1 style="text-align: center; text-transform: uppercase;"><?php echo $cat->name ?></h1>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 gallery" style="margin-top:10rem;">
                                                        <div class="bottom-container">
                                                            <div class="swiper-button-prev swiper-alt-left"></div>
                                                            <h2 class="category-name" style="width: 75%;"><?php echo $previous->name ?></h2>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 image">
                                                        <a href="<?php echo $term_link  ?>">
                                                            <img class="current_image" src="<?php echo $image ?>" alt="<?php echo $image ?>">
                                                        </a>
                                                    </div>

                                                    <div class="col-md-3 desc" style="margin-top:10rem;">
                                                        <div class="right-container">
                                                            <h2 class="category-name w-75" style="width: 75%;"><?php echo $next->name ?></h2>
                                                            <div class="swiper-button-next swiper-alt-right">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <img class="img_right" src="" alt="slider_next">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page_section">
        <div class="container-fluid" style="margin-bottom: -130px;">
            <div class="row">
                <div class="col-12 page_section--content">
                    <div class="width_content">
                        <div class="title_section">
                            <h3>Wyjątkowe zalety</h3>
                            <h2>Co sprawia, że nasze bombki<br> są unikatowe?</h2>
                        </div>
                        <div class="desc">
                            <h4>Nasze bombki są tworzone z sercem.<br> I ogniem.</h4>
                            <p>– dmuchane, ręcznie malowane i zdobione.</p>
                            <h4>Każda z nich kryje w sobie piekną tajemnicę.</h4>
                            <p>Każda z nich jest unikatowym dziełem człowieka.</p>
                            <h4>Sięgnij po swój kawałek rzemieślniczej pasji.</h4>
                            <p>Tradycyjna sztuka wytwarzania wymaga klasycznej precyzji i artyzmu.</p>
                        </div>
                        <noscript>
                            <img src="https://komozja.weblider24.pl/wp-content/uploads/2020/06/ogien-v3.jpg" alt="">
                        </noscript>
                        <img class=" lazyloaded" src="https://komozja.weblider24.pl/wp-content/uploads/2020/06/ogien-v3.jpg" data-src="https://komozja.weblider24.pl/wp-content/uploads/2020/06/ogien-v3.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>




    <?php
    $desc_collections = get_field('desc_tag');
    ?>


    <?php
    $title_collection = get_field('title_collection');
    $term = get_field('cat_collection');
    ?>
    <section class="collection_container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 title_section wow fadeInUp">
                    <?php echo $title_collection ?>
                </div>
                <div class="col-12 slider_container">
                    <?php
                    if ($term) : ?>
                        <div class="swiper-container swiper_collection">
                            <div class="swiper-wrapper">
                                <?php
                                $args = array(
                                    'post_type' => 'product',
                                    'posts_per_page' => -1,
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'product_cat',
                                            'terms' => $term->slug,
                                            'field' => 'slug',
                                            'include_children' => true,
                                            'operator' => 'IN'
                                        )
                                    )
                                );
                                $parent = new WP_Query($args);
                                if ($parent->have_posts()) : ?>
                                    <?php while ($parent->have_posts()) : $parent->the_post();
                                        $_product = wc_get_product(get_the_ID());
                                        $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                                    ?>
                                        <div class="swiper-slide">
                                            <?php
                                            if (!empty($thumb)) { ?>
                                                <img src="<?php echo $thumb['0']; ?>" alt="<?php echo the_title(); ?>">
                                            <?php }
                                            ?>
                                            <div class="coast">
                                                <p><?php echo $_product->get_price(); ?> <?php echo get_woocommerce_currency_symbol(); ?></p>
                                            </div>
                                            <h3><a href="<?php echo get_the_permalink(); ?>"> <?php echo the_title(); ?></a></h3>
                                            <?php if (has_excerpt($post->ID)) { ?>
                                                <div class="excerpt">
                                                    <?php echo the_excerpt(); ?>
                                                </div>
                                            <?php  } else {
                                            }
                                            ?>
                                            <div class="btn">
                                                <a href="<?php echo get_the_permalink(); ?>"><?php echo pll_e('Zobacz produkt'); ?></a>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                <?php endif;
                                wp_reset_query(); ?>
                            </div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
    <?php
    $img_history = get_field('img_history');
    $desc_history = get_field('desc_history');
    $desc_history_2 = get_field('desc_history_2');
    ?>
    <section id="history_container" class="history_container">
        <div class="bg_grey">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 history_container_img">
                        <?php
                        if (!empty($img_history)) { ?>
                            <img src="<?php echo $img_history['url'] ?>" alt="<?php echo $img_history['alt'] ?>">
                        <?php }
                        ?>
                    </div>
                    <div class="col-md-4 history_container_desc wow fadeInRight">
                        <?php echo $desc_history ?>
                        <a href="/historia-marki/" class="btn btn-block button">Poznaj całą naszą historię</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 line_history">
                    <?php if (have_rows('box_history')) : $count = 0; ?>
                        <ul>
                            <?php while (have_rows('box_history')) : the_row();
                                $year_box = get_sub_field('year_box');
                                $title_box = get_sub_field('title_box');
                                $desc_box = get_sub_field('desc_box');
                                $year_array = str_split($year_box, 1);
                                $count++;
                            ?>
                                <li class="single_history wow fadeInUp" data-wow-delay="0.<?php echo $count ?>s">
                                    <a href="<?php echo get_page_link(pll_get_post(19)) ?>/#<?php echo $year_box ?>">
                                        <p class="year">
                                            <?php foreach ($year_array as $number) { ?>
                                                <span><?php echo $number ?></span>
                                            <?php } ?>
                                        </p>
                                        <div class="circle_container">
                                            <span class="circle"></span>
                                        </div>
                                        <p class="title"><?php echo $title_box ?></p>
                                        <p class="desc"><?php echo $desc_box ?></p>
                                    </a>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="history_section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-lg-4 history_title wow fadeInLeft">
                        <?php echo $desc_history_2 ?>
                    </div>
                    <div class="col-md-6 col-lg-8 ">
                    </div>
                </div>
            </div>
            <div class="map">
                <img class="base" src="<?php echo get_stylesheet_directory_uri(); ?>/img/mapa1.jpg" />
                <img class="base_children base_children_1" src="<?php echo get_stylesheet_directory_uri(); ?>/img/mapa2.png" />
                <img class="base_children base_children_2" src="<?php echo get_stylesheet_directory_uri(); ?>/img/mapa3.png" />
            </div>
        </div>
    </section>
    <?php
    $title_stages = get_field('title_stages');
    ?>
    <section class="stages">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="bg">
                        <img class="img_left" src="" alt="slider_prev">
                        <div class="title_section">
                            <?php echo $title_stages ?>
                        </div>
                        <?php if (have_rows('box_stages')) : $count = -1; ?>
                            <div class="swiper-container swiper_stages">
                                <div class="swiper-wrapper">
                                    <?php while (have_rows('box_stages')) : the_row();
                                        $image_box = get_sub_field('image_box');
                                        $desc_box = get_sub_field('desc_box');
                                        $count++;
                                    ?>
                                        <div class="swiper-slide id_<?php echo $count ?>">
                                            <div class="row">
                                                <div class="col-md-3 gallery">
                                                    <?php
                                                    $images = get_sub_field('gallery_box');
                                                    if ($images) : ?>
                                                        <ul>
                                                            <?php foreach ($images as $image) : ?>
                                                                <li>
                                                                    <a data-rel="lightcase" data-rel="lightcase:myCollection2" href="<?php echo esc_url($image['url']); ?>">
                                                                        <img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                                                                    </a>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="col-md-5 image">
                                                    <img class="current_image" src="<?php echo $image_box['url'] ?>" alt="<?php echo $image_box['alt'] ?>">
                                                </div>
                                                <div class="col-md-4 desc">
                                                    <?php echo $desc_box ?>
                                                </div>
                                            </div>

                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        <?php endif; ?>
                        <img class="img_right" src="" alt="slider_next">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    $desc_popular = get_field('desc_popular');
    $img_popular = get_field('img_popular');
    ?>
    <section class="popular_post">
        <div class="grid">
            <div class="single_grid big title_section">
                <div class="wow fadeInLeft">
                    <?php echo $desc_popular ?>
                </div>
                <img src="<?php echo $img_popular['url'] ?>" alt="<?php echo $img_popular['alt'] ?>">
            </div>
            <?php
            $ids = get_field('popular_post', false, false);
            $args = array(
                'post_type' => 'product',
                'post__in'            => $ids,
                'post_status'        => 'any',
                'orderby'            => 'post__in',
            );
            $parent = new WP_Query($args);
            if ($parent->have_posts()) : $countPost = 0; ?>
                <?php while ($parent->have_posts()) : $parent->the_post();
                    $_product = wc_get_product(get_the_ID());
                    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                    $countPost++;
                    if ($countPost == 4) {
                        $class = 'big';
                    } else {
                        $class = 'small';
                    }
                ?>
                    <div class="single_grid <?php echo $class ?> single_product_card">
                        <?php
                        if (!empty($thumb)) { ?>
                            <img src="<?php echo $thumb['0']; ?>" alt="<?php echo the_title(); ?>">
                        <?php }
                        ?>
                        <div class="coast">
                            <p><?php echo $_product->get_price(); ?> <?php echo get_woocommerce_currency_symbol(); ?></p>
                        </div>
                        <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo the_title(); ?></a></h3>
                        <?php if (has_excerpt($post->ID)) { ?>
                            <div class="excerpt">
                                <?php echo the_excerpt(); ?>
                            </div>
                        <?php  } else {
                        }
                        ?>
                        <div class="btn">
                            <a href="<?php echo get_the_permalink(); ?>"><?php echo pll_e('Zobacz produkt'); ?></a>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif;
            wp_reset_query(); ?>
            <div class="single_grid small all_post single_product_card">
                <a href="<?php echo get_page_link(pll_get_post(216)) ?>">
                    <h3><?php echo pll_e('Zobacz wszystkie produkty') ?></h3>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ikona-wymiar-szerokosc2@2.png" />
                </a>
            </div>
        </div>
    </section>
</main>
<?php get_footer(); ?>