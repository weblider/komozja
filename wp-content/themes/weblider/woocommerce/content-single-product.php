<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js"></script>
<div id="product-<?php the_ID(); ?>" class="product_container">

	<?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	//do_action( 'woocommerce_before_single_product_summary' );
	?>
	<?php
	$desc_scroll = get_field( 'desc_scroll' );
	$product   = wc_get_product( get_the_ID() );
	$image_id  = $product->get_image_id();
	$image_url = wp_get_attachment_image_url( $image_id, 'full' );
	
	// foreach( $product->get_gallery_image_ids() as $attachment_id ) {
        // echo $image_link = wp_get_attachment_url( $attachment_id );
    // }
	
	
	?>
	<div class="scroll_container">
		<div class="produkt_front produkt_front_fixed">
			<div class="bg">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12 title_page ">
							<h2><?php echo pll_e( 'Charakterystyka' );?></h2>
						</div>
						<div class="col-md-4 desc_left">
							<?php echo $desc_scroll ?>
						
						</div>
						<div class="col-md-4 thumb">
							<?php
							$product   = wc_get_product( get_the_ID() );
							$image_id  = $product->get_image_id();
							$image_url = wp_get_attachment_image_url( $image_id, 'full' );
							 ?>
							 <div class="thumb_detail" >
								 <?php
								 if( !empty( $product->get_height() ) ) { ?>
									 <div class="height">
										 <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ikona-wymiar-wysokosc1@2.png" />
										 <p><?php echo $product->get_height(); ?> <?php echo get_option( 'woocommerce_dimension_unit' ) ?> </p>
										 <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ikona-wymiar-wysokosc2@2.png" />
									 </div>
								 <?php }
								 ?>
								<img class="image" src="<?php echo $image_url ?>" alt="<?php echo get_the_title(); ?>">
								<?php
								if( !empty( $product->get_width() ) ) { ?>
									<div class="width">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ikona-wymiar-szerokosc1@2.png" />
										<p><?php echo $product->get_width(); ?> <?php echo get_option( 'woocommerce_dimension_unit' ) ?></p>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ikona-wymiar-szerokosc2@2.png" />
									</div>
								<?php }
								?>
								<?php
								if( !empty( $product->get_length() ) ) { ?>
									<div class="length">
										<img class="rotate_1" src="<?php echo get_stylesheet_directory_uri(); ?>/img/ikona-wymiar-szerokosc1@2.png" />
										<p><?php echo $product->get_length(); ?> <?php echo get_option( 'woocommerce_dimension_unit' ) ?></p>
										<img class="rotate_2" src="<?php echo get_stylesheet_directory_uri(); ?>/img/ikona-wymiar-szerokosc2@2.png" />
									</div>
								<?php }
								?>
							 </div>
						</div>
						<div class="col-md-4 small_desc">
							<div class="desc">
								<?php
								if( !empty( $product->get_sku() ) ) { ?>
									<div class="code_product">
										<p><?php echo pll_e('Nr katalogowy:');?> <?php echo $product->get_sku() ?></p>
									</div>
								<?php }
								?>
								<div class="coast">
		<?php
		$price_excl_tax = wc_get_price_excluding_tax( $product );
		if( !empty( $price_excl_tax ) ) {
			 $_product = wc_get_product( get_the_ID() ); ?>
			<p><?php echo $_product->get_price(); ?> <?php echo get_woocommerce_currency_symbol(); ?></p>
		<?php }
		?>
								</div>
								<h1><?php echo get_the_title(); ?></h1>
								<?php
								 woocommerce_template_single_add_to_cart();
								?>

							</div>
						</div>
					</div>
				</div>
				<div class="scroll scrollHref">
					<a href="#collections_container">
						<?php echo pll_e( 'Poznaj nas' );?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/strzalka@2.png" />
					</a>
				</div>
				<div class="breadcrumbs_container">
					<?php //bcn_display($return = false, $linked = true, $reverse = false, $force = false) ?>
				</div>
			</div>
		</div>
	</div>
	<section class="page_box">
		<div class="container-fluid">
			<div class="row">
				<?php if( have_rows('box_page') ): ?>
				   <?php while( have_rows('box_page') ): the_row();
				       $title_box = get_sub_field('title_box');
				       $desc_box = get_sub_field('desc_box');
				       $img_box = get_sub_field('img_box');
				       $img_left_right = get_sub_field('img_left_right');
					   if( $img_left_right ) {
						   $class = 'content_right';
					   } else {
						   $class = 'content_left';
					   }
				        ?>
				          <div class="col-12 single_box <?php echo $class ?>">
				          	<div class="bg" style="background-image: url(<?php echo $img_box['url'] ?>)">
				          		<div class="title_section">
				          			<?php echo $title_box ?>
				          		</div>
								<div class="single_box--content">
									<?php echo $desc_box ?>
								</div>
				          	</div>
				          </div>
				  <?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
	
	<section class="page_gallery">
	<div class="container">
		<h2 style="text-align: center;
    font-size: 2.75rem;
    text-transform: uppercase; color: #333;">Galeria zdjęć </h6>
		<div class="row">
		<?php if($product->get_gallery_image_ids()) { ?>
		<?php foreach( $product->get_gallery_image_ids() as $attachment_id ) { ?>
		<div class="col-sm">
			<a href="<?php echo wp_get_attachment_url( $attachment_id ); ?>" data-lightbox="roadtrip"><img style="background: #E7E7E7; border-radius: 12px;" data-lightbox="roadtrip" src="<?php echo wp_get_attachment_url( $attachment_id ); ?>"/></a>
		</div>
		<?php } ?>
		<?php } ?>
			
		
	</div>
	</section>
	
	<?php
	$title_section = get_field( 'title_section' );
	$desc_section = get_field( 'desc_section' );
	$image_section = get_field( 'image_section' );
	?>
	<section class="page_section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 page_section--content">
					<div class="width_content">
						<div class="title_section">
							<?php echo $title_section ?>
						</div>
						<div class="desc">
							<?php echo $desc_section ?>
						</div>
						<?php
						if( !empty( $image_section ) ) { ?>
							<img src="<?php echo $image_section['url'] ?>" alt="<?php echo $image_section['alt'] ?>">
						<?php }
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php
	$popular_title = get_field( 'popular_title' );
	?>
	<div class="popular_single">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 title_section">
					<?php echo $popular_title ?>
				</div>
			</div>
		</div>
		<div class="grid">
			<?php
			$args = array(
			   'post_type' => 'product',
			   'posts_per_page' => 4,
			   'order'          => 'ASC',
			   'orderby'        => 'menu_order'
			 );
			$parent = new WP_Query( $args );
			if ( $parent->have_posts() ) : ?>
			   <?php while ( $parent->have_posts() ) : $parent->the_post();
			   $_product = wc_get_product( get_the_ID() );
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			    ?>
				   <div class="single_product_card">
					   <?php
					   if( !empty( $thumb ) ) { ?>
						   <img src="<?php echo $thumb['0'];?>" alt="<?php echo the_title(); ?>">
					   <?php }
					   ?>
					   <div class="coast">
						   <p><?php echo $_product->get_price(); ?> <?php echo get_woocommerce_currency_symbol(); ?></p>
					   </div>
					   <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo the_title(); ?></a></h3>
					   <?php if ( has_excerpt( $post->ID ) ) { ?>
						  <div class="excerpt">
							  <?php  echo the_excerpt(); ?>
						  </div>
					   <?php  } else {
						}
						?>
						<div class="btn">
							<a href="<?php echo get_the_permalink(); ?>"><?php echo pll_e( 'Zobacz produkt' );?></a>
						</div>
				   </div>

			   <?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>
		</div>
	</div>


	<!-- <div class="summary entry-summary"> -->
		<?php
		/**
		 * Hook: woocommerce_single_product_summary.
		 *
		 * @hooked woocommerce_template_single_title - 5
		 * @hooked woocommerce_template_single_rating - 10
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_excerpt - 20
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * @hooked woocommerce_template_single_meta - 40
		 * @hooked woocommerce_template_single_sharing - 50
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 */
		//do_action( 'woocommerce_single_product_summary' );

		?>
	<!-- </div> -->

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	//do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>

<?php //do_action( 'woocommerce_after_single_product' ); ?>
