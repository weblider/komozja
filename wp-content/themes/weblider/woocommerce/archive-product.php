<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header(  ); ?>
<main>
	<?php
	$cat = get_queried_object();
	$opinion_desc = get_field( 'opinion_desc', $cat );
	$name_opinion = get_field( 'name_opinion', $cat );
	$thumb_cat = get_field( 'thumb_cat', $cat );
	?>
	<?php
	if( is_search() || is_shop()) {
		$class = 'small_header';
	} else {
		$class = '';
	}
	?>
	<section class="archive_header <?php echo $class ?>">
		<div class="header_desc">
			<?php
			if( is_product_category() ) { ?>
				<h1 class="wow fadeInUp"><?php echo single_cat_title() ?></h1>
				<div class="wow fadeInUp">
					<?php echo category_description() ?>
				</div>
			<?php } else if ( is_product_tag() ) { ?>
				<h1 class="wow fadeInUp"><?php echo single_tag_title() ?></h1>
				<div class="wow fadeInUp">
					<?php echo tag_description(); ?>
				</div>
			<?php } else if (is_search() ) {
				$title=$_GET['s']; ?>
				<h1 class="wow fadeInUp"><?php echo pll_e( 'Wyszukiwanie:' );?> <?php echo $title ?> </h1>
			<?php } else if ( is_shop() ){ ?>
				<h1 class="wow fadeInUp"><?php echo woocommerce_page_title(); ?> </h1>
			<?php } else {

			}
			?>
		</div>
		<?php
		if( !empty( $thumb_cat ) ) { ?>
			<img src="<?php echo $thumb_cat['url'] ?>" alt="<?php echo single_cat_title() ?>">
		<?php }
		?>

	</section>
	<section class="archive_content">
		
		<div class="grid">
			<?php
				if ( have_posts() ) : $count = 0;?>
					<?php while ( have_posts() ) : the_post();
						global $product;
					$_product = wc_get_product( get_the_ID() );
					 $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					 $count++;
					 if( $count == 2 || $count == 6 || $count == 8 || $count == 9 || $count == 15 ) {
						 if( !empty( $opinion_desc ) &&  $count == 8 ) {
							 $class = 'small';
						 } else {
							 $class = 'big';
						 }
					 } else {
						 $class = 'small';
					 }
					 if( $count == 8 && !is_shop()) {
					 	if( !empty( $opinion_desc ) ) { ?>
					 		 <div class="single_grid big opinion">
					 			 <div class="desc">
					 			 	<?php echo $opinion_desc ?>
					 			 </div>
					 			<p class="name"><?php echo $name_opinion ?></p>
					 		 </div>
					 	<?php }
					  ?>
					 <?php
				 	}
					?>
					<div class="single_grid <?php echo $class ?> <?php echo $count ?> single_product_card">
						<?php
						if( !empty( $thumb ) ) { ?>
							<img src="<?php echo $thumb['0'];?>" alt="<?php echo the_title(); ?>">
						<?php }
						?>
						<div class="desc">
							<div class="coast">
								<?php
								$price_excl_tax = wc_get_price_excluding_tax( $product );
								if( !empty( $price_excl_tax ) ) { ?>
									<p><?php echo $_product->get_price(); ?> <?php echo get_woocommerce_currency_symbol(); ?></p>
								<?php }
								?>
							</div>
							<h3><?php echo the_title(); ?></h3>
							<?php if ( has_excerpt( $post->ID ) ) { ?>
							   <div class="excerpt">
								   <?php  echo the_excerpt(); ?>
							   </div>
							<?php  } else {
							 }
							 ?>
							 <div class="btn">
								 <a href="<?php echo get_the_permalink(); ?>"><?php echo pll_e( 'Zobacz produkt' );?></a>
							 </div>
						</div>
					</div>
					<?php
					if( $count == 15 ) {
						$count = 0;
					}
					?>
					<?php endwhile; ?>
					<?php else : ?>
						<div class="single_grid">
							<div class="desc">
								<h2><?php echo pll_e( 'Przepraszamy, brak produktów spełniające podane kryteria' );?></h2>
							</div>
						</div>
				<?php endif; ?>
		</div>
	</section>
</main>
<?php get_footer(  );
