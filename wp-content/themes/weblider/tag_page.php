<?php

/* Template name: Formy */

?>

<?php get_header(); ?>

 <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

<main>

    <section class="archive_header">

        <?php

            if ( have_posts() ) : ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <div class="content wow fadeInUp">

                        <?php echo get_the_content(); ?>

                    </div>

                    <?php

                    if( !empty( $thumb ) ) { ?>

                    <img src="<?php echo $thumb['0']?>" alt="<?php echo get_the_title();?>">

                    <?php }

                    ?>

                <?php endwhile; ?>

            <?php endif; ?>

    </section>

    <div class="breadcrumbs_container">

        <?php bcn_display($return = false, $linked = true, $reverse = false, $force = false) ?>

    </div>

    <section class="all_cat">

        <?php

        $terms = get_terms( array(

            'hide_empty' => false, // only if you want to hide false

            'taxonomy' => 'product_tag',

            )

        );

        $count = 0;

        foreach( $terms as $term ) {

             $icon_tag = get_field( 'icon_tag', 'term_' . $term->term_taxonomy_id );

            $count++;

            if( $term->slug == 'bez-kategorii' ) {



            } else {

                if( $count == 1  || $count == 3) {

                    $class = 'down';

                } else {

                    $class = 'up';

                }

                ?>

                <div class="single_cat <?php echo $class ?> single_tag--loop">

                    <a href="<?php echo get_category_link($term->term_id) ?>">

                        <div class="desc desc_up">

                            <h3><?php echo $term->name; ?></h3>

                            <p><?php echo $term->description; ?></p>

                        </div>

                        <img style="height:100%"  src="<?php echo $icon_tag['url'] ?>" alt="<?php echo $term->name; ?>">

                        <div class="desc desc_down">

                            <h3><?php echo $term->name; ?></h3>

                            <p><?php echo $term->description; ?></p>

                        </div>

                    </a>

                </div>

            <?php }

            if( $count == 3 ) {

                $count = 0;

            }

            ?>

        <?php } ?>

    </section>

</main>

<?php get_footer();

