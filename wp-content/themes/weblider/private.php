<?php

/* Template name: Prywatna kolekcja */

?>

<?php get_header(); ?>
<style>
    .row{
        padding:0;
        margin: 0 0;
    }
</style>

<main>
    <section class="archive_header small_header">
        <div class="header_desc ">
            <?php
            if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
                    <h1 class="wow fadeInUp"><?php echo get_the_title(); ?></h1>
                    <div class="wow fadeInUp">
                        <?php echo get_the_content(); ?>
                    </div>
                    <img src="<?php echo $thumb['0'];?>'" alt="">
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </section>
  <div class="container">
      <div class="row" style="align-items: center; justify-content: center; text-align: center;">
          <div class="col-lg-6 text-center order-sm-0 order-lg-0" style="background: #e7e7e7; padding: 5%;">
              <img style="max-height: 400px;  margin: 0 auto" src="http://komozja.weblider24.pl/wp-content/uploads/2020/12/2193K00_side3.png" />
          </div>
          <div class="col-lg-6 order-sm-1 order-lg-1" style="padding:5%;" >
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
          </div>
      </div>

      <div class="row"  style="align-items: center; justify-content: center; text-align: center;">
          <div class="col-lg-6 order-sm-1 order-lg-0" style="padding:5%;" >
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
          </div>
          <div class="col-lg-6 order-sm-0 order-lg-1"  style="background: #e7e7e7; padding: 5%;">
              <img style="max-height: 400px; margin: 0 auto" src="http://komozja.weblider24.pl/wp-content/uploads/2020/12/2193K00_side3.png" />
          </div>
      </div>

  </div>
</main>

<?php get_footer();

