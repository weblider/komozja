<?php
/* Template name: Nowości */
?>
<?php get_header(); ?>

<section class="archive_header small_header">
    <div class="header_desc ">
        <?php
            if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
                    <h1 class="wow fadeInUp"><?php echo get_the_title(); ?></h1>
                    <div class="wow fadeInUp">
                      <?php echo get_the_content(); ?>
                    </div>
                    <img src="<?php echo $thumb['0'];?>'" alt="">
                <?php endwhile; ?>
            <?php endif; ?>
    </div>
</section>
<div class="popular_post">
    <div class="grid">
<?php
$args = array(
    'post_type' => 'product',
   'posts_per_page' => 20,
   'order'          => 'ASC',
   'orderby'        => 'menu_order'
 );
$parent = new WP_Query( $args );
if ( $parent->have_posts() ) : ?>
   <?php while ( $parent->have_posts() ) : $parent->the_post();
   $_product = wc_get_product( get_the_ID() );
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
   ?>
       <div class="single_grid <?php echo $class ?> single_product_card">
           <?php
           if( !empty( $thumb ) ) { ?>
               <img src="<?php echo $thumb['0'];?>" alt="<?php echo the_title(); ?>">
           <?php }
           ?>
           <div class="coast">
               <p><?php echo $_product->get_price(); ?> <?php echo get_woocommerce_currency_symbol(); ?></p>
           </div>
           <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo the_title(); ?></a></h3>
           <?php if ( has_excerpt( $post->ID ) ) { ?>
              <div class="excerpt">
                  <?php  echo the_excerpt(); ?>
              </div>
           <?php  } else {
            }
            ?>
            <div class="btn">
                <a href="<?php echo get_the_permalink(); ?>"><?php echo pll_e( 'Zobacz produkt' );?></a>
            </div>
       </div>
   <?php endwhile; ?>
<?php endif; wp_reset_query(); ?>
</div>
</div>
<?php get_footer(); ?>
