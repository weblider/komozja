
<?php get_header(); ?>
<?php
$thumb__post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
	<div class="wrapper-search">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-lg-9 list__prod">
					<div class="row">
		            <?php
		             $title=$_GET['s']
		            ?>
		            <?php
		            $args = array(
		               'post_type'      => 'product',
		               'posts_per_page' => -1,
		               's'              => $title
		             );
		            $parent = new WP_Query( $args );
		            if ( $parent->have_posts() ) : ?>
		               <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
		                 <div class="products__box__single col-md-4">
		                   <a href="<?php echo the_permalink(); ?>">
		                     <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		                     ?>
		                      <?php
		                      if( !empty( $thumb ) ) { ?>
		                         <img class="single__content__bg" src="<?php echo $thumb['0'] ?>">
		                      <?php }
		                      ?>
		                     <div class="single__content__title">
		                       <p><?php echo the_title(); ?></p>
		                     </div>
		                   </a>
		                 </div>
		               <?php endwhile;
		             else: ?>
		             <p><?php echo pll__( 'Przepraszamy, brak wpisów spełniających podane kryteria.' ); ?></p>
		            <?php endif; wp_reset_query(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
