<?php

/* Template name: Gdzie kupić */

?>

<?php get_header(); ?>

<?php

    $title_page = get_field( 'title_page' );

?>

<div class="where_buy">

    <div id="map">



    </div>

    <div class="where_header">

        <div class="container-fluid">

            <div class="row">

                <div class="col-md-8">



                </div>

                <div class="col-md-4 desc wow fadeInRight">

                <?php echo $title_page?>

                </div>

            </div>

        </div>

    </div>

    <?php

    $title_section = get_field( 'title_section' );

    ?>

    <div class="where_store">

        <div class="container-fluid">

            <div class="row">

                <div class="col-12 title_section wow fadeInUp">

                    <?php echo $title_section ?>

                </div>

                <div class="col-12 where_repeater">

                    <?php if( have_rows('box_repeater') ): $count=0;?>

                       <?php while( have_rows('box_repeater') ): the_row();

                           $icon_box = get_sub_field('icon_box');

                           $title_box = get_sub_field('title_box');

                           $desc_box = get_sub_field('desc_box');
                           $count++;

                            ?>

                              <div class="single_repeater wow fadeInUp" data-wow-delay="0.<?php echo $count ?>s">

                                  <img src="<?php echo $icon_box['url'] ?>" alt="<?php echo $icon_box['alt'] ?>">

                                  <h4><?php echo $title_box ?></h4>

                                  <?php echo $desc_box ?>

                              </div>

                      <?php endwhile; ?>

                    <?php endif; ?>

                </div>

            </div>

        </div>

    </div>

    <?php

    $title_section_2 = get_field( 'title_section_2' );

    $bg_section_2 = get_field( 'bg_section_2' );

    ?>

    <div class="text_image">

        <div class="bg" style="background-image: url(<?php echo $bg_section_2['url']?>)">

            <div class="wow fadeInUp"> 
    <?php echo $title_section_2 ?>
            </div>

        </div>

    </div>

    <?php

    $title_section_3 = get_field( 'title_section_3' );

    $image_person = get_field( 'image_person' );

    $desc_person = get_field( 'desc_person' );

    $name_person = get_field( 'name_person' );

    $email_person = get_field( 'email_person' );

    $phone_person = get_field( 'phone_person' );

    ?>

    <div class="cooperation">

        <div class="container-fluid">

            <div class="row">

                <div class="col-md-7 cooperation_title wow fadeInLeft">

                    <?php echo $title_section_3 ?>

                </div>

                <div class="col-md-5 cooperation_box">

                    <div class="content_box wow fadeInRight">

                        <img src="<?php echo $image_person['url'] ?>" alt="<?php echo $name_person?>">

                        <?php echo $desc_person ?>

                        <hr>

                        <p class="name"><?php echo $name_person ?></p>

                        <a class="email" href="mailto:<?php echo $email_person ?>"><?php echo $email_person ?></a>

                        <a class="tel" href="tel:<?php echo phone_to_link($phone_person)?>">tel: <?php echo $phone_person ?></a>

                        <hr>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="shop__list">

    <?php if( have_rows('shop__location') ): ?>

       <?php while( have_rows('shop__location') ): the_row();

           $lat = get_sub_field('lat');

           $lng = get_sub_field('lng');

           $title__business = get_sub_field('title__business');

           $web__business = get_sub_field('web__business');

           $text__btn__website = get_sub_field('text__btn__website');

           $email = get_sub_field('email');

           $tel = get_sub_field('tel');

           $street = get_sub_field('street');

           $code_post = get_sub_field('code_post');

           $city = get_sub_field('city');

            ?>

              <div class="single"

               lat="<?php echo $lat ?>"

               lng="<?php echo $lng ?>"

               title__business="<?php echo $title__business ?>"

               web__business="<?php echo $web__business ?>"

               text__btn__website="<?php echo $text__btn__website ?>"

               email="<?php echo $email ?>"

               tel="<?php echo $tel ?>"

               street="<?php echo $street ?>"

               code_post="<?php echo $code_post ?>"

               city="<?php echo $city ?>"

               >



              </div>

      <?php endwhile; ?>

    <?php endif; ?>

</div>

<script>

var locations = [];

$( '.shop__list .single' ).each(function( index ) {

    var lat = $( this ).attr('lat');

    var lng = $( this ).attr('lng');

    var title__business = $( this ).attr('title__business');

    var tel = $( this ).attr('tel');

    var web__business = $( this ).attr('web__business');

    var text__btn__website = $( this ).attr('text__btn__website');

    var street = $( this ).attr('street');

    var email = $( this ).attr('email');

    var code_post = $( this ).attr('code_post');

    var city = $( this ).attr('city');

    var singleList =  {

        lng: parseFloat(lng),

        lat: parseFloat(lat),

        title__business: title__business,

        tel: tel,

        email: email,

        web__business: web__business,

        text__btn__website: text__btn__website,

        street: street,

        code_post: code_post,

        city: city

    }

 locations.push(singleList);

});



	function initMap() {

        geocoder = new google.maps.Geocoder();

		var uluru = {lat: -31.563910, lng: 147.154312};



		var styledMapType = new google.maps.StyledMapType(

			[

			    {

			        "featureType": "all",

			        "elementType": "all",

			        "stylers": [

			            {

			                "hue": "#008eff"

			            }

			        ]

			    },

			    {

			        "featureType": "poi",

			        "elementType": "all",

			        "stylers": [

			            {

			                "visibility": "off"

			            }

			        ]

			    },

			    {

			        "featureType": "road",

			        "elementType": "all",

			        "stylers": [

			            {

			                "saturation": "0"

			            },

			            {

			                "lightness": "0"

			            }

			        ]

			    },

			    {

			        "featureType": "transit",

			        "elementType": "all",

			        "stylers": [

			            {

			                "visibility": "off"

			            }

			        ]

			    },

			    {

			        "featureType": "water",

			        "elementType": "all",

			        "stylers": [

			            {

			                "visibility": "simplified"

			            },

			            {

			                "saturation": "-60"

			            },

			            {

			                "lightness": "-20"

			            }

			        ]

			    }

			],

					 {name: 'Styled Map'});



		var map = new google.maps.Map(document.getElementById('map'), {

			zoom: 5,

			center: uluru,

			disableDefaultUI: true

		});

        var infoWin = new google.maps.InfoWindow();

		var image = {

			url: rest_url.theme+'/img/lokalizacja.png',

    		scaledSize: new google.maps.Size(38, 55) // scaled size

        };

        $( '.where .btn_location' ).click( function(){

            $( '.where .search_map' ).css( 'display', 'flex' );

            if (navigator.geolocation) {

              navigator.geolocation.getCurrentPosition(function(position) {

                var pos = {

                  lat: position.coords.latitude,

                  lng: position.coords.longitude

                };

                map.setCenter(new google.maps.LatLng(pos.lat, pos.lng));

                map.setZoom(13);

                // infoWin.setPosition(pos);

                // infoWin.setContent('Tutaj jesteś');

                // infoWin.open(map);

              }, function() {



              });

            } else {

            }

        } )







        var markers = locations.map(function(location, i) {

          var marker = new google.maps.Marker({

            position: {lat: location.lat, lng: location.lng},

            icon: image,

          });







          var contentString =

   '<div id="contentWindow"><h2>'+ location.title__business +'</h2><p>'+ location.street +'</p><p>'+ location.code_post +' '+ location.city +'</p><p>tel.'+ location.tel +'</p><p>mail. <a href="mailto:'+location.email+'">'+ location.email +'</a></p><a class="maplink" href="'+ location.web__business +'">'+ location.text__btn__website +'</a>   </div>';



          google.maps.event.addListener(marker, 'click', function(evt) {

            infoWin.setContent(contentString);

            infoWin.open(map, marker);

          })

          return marker;

        });



     var clusterStyles = [

  {

    textColor: 'white',

    textSize:16,

    url: rest_url.theme+'/img/cluster/m1.png',

    height: 55,

    width: 54

  },

 {

    textColor: 'white',

    textSize:16,

    url: rest_url.theme+'/img/cluster/m1.png',

    height: 55,

    width: 54

  },

 {

    textColor: 'white',

    textSize:16,

    url: rest_url.theme+'/img/cluster/m1.png',

    height: 55,

    width: 54

  }

];

     var options = {

         styles: clusterStyles,

};

        var markerCluster = new MarkerClusterer(map, markers, options);

		map.mapTypes.set('styled_map', styledMapType);

		map.setMapTypeId('styled_map');

	}









    function codeAddress() {

      var address = document.getElementById('address').value;

      console.log(address);

      geocoder.geocode( { 'address': address}, function(results, status) {

          console.log(results);

        if (status == 'OK') {

          map.setCenter(results[0].geometry.location);

          var marker = new google.maps.Marker({

              map: map,

              position: results[0].geometry.location

          });

        } else {

          alert('Geocode was not successful for the following reason: ' + status);

        }

      });

    }



</script>

<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">

   </script>

<script async defer

src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap">

</script>

<?php get_footer();

