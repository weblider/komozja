<?php

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://codex.wordpress.org/Editing_wp-config.php

 *

 * @package WordPress

 */


// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define('DB_NAME', "local");


/** MySQL database username */

define('DB_USER', "root");


/** MySQL database password */

define('DB_PASSWORD', "root");


/** MySQL hostname */

define('DB_HOST', "localhost");


/** Database Charset to use in creating database tables. */

define('DB_CHARSET', 'utf8mb4');


/** The Database Collate type. Don't change this if in doubt. */

define('DB_COLLATE', '');


/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define('AUTH_KEY',         'TfL`3;^hU5XTAe{)_7PpCjD+( ?*1@iU~q#>2h,[+U[kd$jgusRU]^EPl}yw|zB+');

define('SECURE_AUTH_KEY',  'JuIgJ:7SBcl_ro9F F`RK_62(y1`40,];N.oyFU%M.y Jc>^~esBP* ?M%MRm{=[');

define('LOGGED_IN_KEY',    'zcJHY{C%-ZKAW2X~we2H+8XrhMxXmXpJNSgD4#zqOh#mJ.3]*0,Mi#!5YZo n]_Q');

define('NONCE_KEY',        '~EbpoNeXbz({WX9.B2FIpH94Tq|pYoXes#f(!MY|%475fEqENP7Vi:MH7[.IZLST');

define('AUTH_SALT',        't[?VK4?[#O|s93WSJj.J~(I9SRaWAt8oqlP-3O@O~QrcO[MRaVv`yK.<Ia,{h&_H');

define('SECURE_AUTH_SALT', 'mkQ[hT/8EmPPZg)$ph~%+FG,5x<oODrI{;[)7+#Y4jgv#0G?zK(!4z(_Sr^8uX.@');

define('LOGGED_IN_SALT',   '%93C]T`hc]3`L?WS7df]%Z,x_@T+o9Iz0k[d>d_&~MIbE*JY@y^[*|b[ByU:O^cI');

define('NONCE_SALT',       'r!K}0lX97>9=-|d`)M63w< ^nWK`ol0y%6bjt`cGGHB8V$5yB)DA.F4tZjw=6@mx');


/**#@-*/


/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix = 'wp_';


/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the Codex.

 *

 * @link https://codex.wordpress.org/Debugging_in_WordPress

 */

define('WP_DEBUG', false);


/* That's all, stop editing! Happy publishing. */


/** Absolute path to the WordPress directory. */

if (!defined('ABSPATH')) {

	define('ABSPATH', dirname(__FILE__) . '/');
}


/** Sets up WordPress vars and included files. */

require_once(ABSPATH . 'wp-settings.php');
